import json
import sys

from middleware.middleware import InsufficientFunds, Middleware


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Pass a .json file containing the chain to replay.')
        sys.exit()

    chain = None
    with open(sys.argv[1]) as f:
        chain = json.loads(f.read())

    mw = Middleware(chain[0]['balances'], chain[0]['hash'])

    for block in chain[1:]:
        mw.process_block(block['hash'], block['prevhash'], block['transfers'])

    print(f'Replayed {len(chain)} blocks. The account balances as of the last block are {mw.accounts}')
