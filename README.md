# UTXO Middleware

Middleware that manages unspent outputs of a blockchain with probabilistic finality.

## Prerequisites

Tested on MacOS with Python 3.6.5.

## Run tests

```
python3 -m unittest middleware/test_middleware.py
```

## Replay chains

You can find a sample chain inside `example.json`.

To replay it, run:

```
python3 replay_from_file.py example.json
```

## On design

The data structures maintained by the middleware are the following:

- An associative array (a dictionary) mapping accounts to their longest chain balances. Allows to retrieve a balance of
  any account at any time in ~O(1);
- A linked list of account states, one state per block. Allows to efficiently update or reorganise the chain. Under
  the limited memory constraints, allows for efficient partial load from disk. Reorg in O(number of reverted blocks);
- The linked list from the previous bullet point also contains a set of transaction identifiers per block. Allows to
  find the number of confirmations in the longest chain for the given transaction in O(number of confirmation blocks);
  An index might be introduced to speed up the lookup of very old or absent transactions. Such index needs to be carefully
  rebuilt upon reorg.

## On limited memory

The memory footprint scales linearly with the number of blocks. Under the limited memory constraint, only the last n
blocks can be kept in memory at any time. Upon reorg or a transaction search request, as soon as these n blocks are
scanned, the linked list can be cut and extended with a new portion of blocks loaded from disk. The linked list avoids
the memory resize overhead.

The transaction index, if introduced, should be maintained on disk. Databases usually offer such indexes.

## About other limitations

- Transactions should have identifiers. For demonstration purposes, transactions are recorded as `(sender, receiver,
  amount)`, but such a triplet does not uniquely identify a transaction.
- The middleware only processes transfers across accounts known at genesis, it does not support adding new accounts.
  However, the `update_accounts` method
  can be used to adjust the amount of a single account, if needed.
- The middleware does not back up its state. In case of a crash, it would need to rebuild its state from the beginning
  of the chain.
- The middleware only maintains data necessary to answer particular queries, like "what are account balances?", "what is their
  median?", "how many confirmations has been received by a particular transaction?". The complete blockchain is
  supposed to be recorded elsewhere.
- The middleware should be used in conjunction with software that receives new blocks from the public network and
  decides on the longest chain, e. g. by calculating the biggest amount of work put into it as in PoW.
- The middleware only offers the native Python interface. An HTTP server can be implemented to allow for broader set of
  clients and use cases. A WebSocket based feed may be used to inform clients about updates.
- The middleware does not verify the integrity of the chain, neither does it deal with any signatures. It is supposed
  to be used by trusted components taking care of these tasks.
- The middleware processes transactions in the order they appear in the block. A successful processing of the block
  might thus depend on the order for the same set of transactions.
