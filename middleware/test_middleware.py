import unittest

from .middleware import AccountDoesNotExist, BlockDoesNotExist, InsufficientFunds, Middleware


class TestMiddleware(unittest.TestCase):

    def test_update_accounts_raises_account_does_not_exist(self):
        mw = Middleware({}, '')
        with self.assertRaises(AccountDoesNotExist):
            mw.update_accounts('1', {'A': 2}, ['tx-1'])

    def test_update_accounts_raises_insufficient_funds(self):
        mw = Middleware({'A': 0}, '0')
        with self.assertRaises(InsufficientFunds):
            mw.update_accounts('1', {'A': -1}, ['tx-1'])

        mw.update_accounts('1', {'A': 5}, ['tx-1'])
        with self.assertRaises(InsufficientFunds):
            mw.update_accounts('2', {'A': -6}, ['tx-1'], '1')

        mw.update_accounts('2', {'A': -5}, ['tx-1'], '1')
        mw.update_accounts('3', {'A': 1}, ['tx-2'], '2')
        with self.assertRaises(InsufficientFunds):
            mw.update_accounts('6', {'A': -2}, ['tx-3'], '3')

    def test_update_accounts_raises_block_does_not_exist(self):
        mw = Middleware({'A': 0}, '0')

        with self.assertRaises(BlockDoesNotExist):
            mw.update_accounts('', {'A': 2}, ['tx-1'], '1')

        mw.update_accounts('1', {'A': 5}, ['tx-1'])
        with self.assertRaises(BlockDoesNotExist):
            mw.update_accounts('3', {'A': 2}, ['tx-2'], '2')

    def test_update_accounts_reorganises_chain(self):
        mw = Middleware({'A': 7, 'B': 3, 'C': 0}, '0')

        assert mw.get_median() == 3

        mw.update_accounts('1', {'A': -6, 'B': +6}, ['tx-1', 'tx-2'], '0')

        assert mw.get_median() == 1

        # reorganise the chain
        mw.update_accounts('1', {'A': -2, 'B': +2}, ['tx-3', 'tx-4'], '0')

        assert mw.get_median() == 5

    def test_process_block_processes_empty_block(self):
        mw = Middleware({'A': 0}, '0')

        mw.process_block('1', '0', [])

        assert mw.get_median() == 0

    def test_process_block_updates_accounts(self):
        mw = Middleware({'A': 7, 'B': 3, 'C': 0}, '0')

        assert mw.get_median() == 3
        assert mw.get_number_of_confirmations_since(('B', 'C', 1)) == 0

        mw.process_block('2', '0', [{'sender': 'B', 'receiver': 'C', 'amount': 1}])
        assert mw.get_median() == 2
        assert mw.get_number_of_confirmations_since(('B', 'C', 1)) == 1

        mw.process_block('3', '2', [{'sender': 'A', 'receiver': 'C', 'amount': 1},
                                    {'sender': 'A', 'receiver': 'B', 'amount': 1}])
        assert mw.get_median() == 3
        assert mw.get_number_of_confirmations_since(('B', 'C', 1)) == 2
        assert mw.get_number_of_confirmations_since(('A', 'C', 1)) == 1
        assert mw.get_number_of_confirmations_since(('A', 'B', 1)) == 1

        # reorganise the chain
        mw.process_block('2', '0', [{'sender': 'B', 'receiver': 'C', 'amount': 1}])
        assert mw.get_median() == 2
        assert mw.get_number_of_confirmations_since(('B', 'C', 1)) == 1
        assert mw.get_number_of_confirmations_since(('A', 'C', 1)) == 0

    def test_process_blocks_accumulates_updates_from_repeated_transfers(self):
        mw = Middleware({'A': 7, 'B': 3, 'C': 0}, '0')

        assert mw.get_median() == 3

        mw.process_block('2', '0', [{'sender': 'A', 'receiver': 'B', 'amount': 5},
                                    {'sender': 'B', 'receiver': 'C', 'amount': 2},
                                    {'sender': 'B', 'receiver': 'C', 'amount': 4}])
        assert mw.get_median() == 2
        assert mw.get_number_of_confirmations_since(('A', 'B', 5)) == 1
        assert mw.get_number_of_confirmations_since(('B', 'C', 2)) == 1
        assert mw.get_number_of_confirmations_since(('B', 'C', 4)) == 1
