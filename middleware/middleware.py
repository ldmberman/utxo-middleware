import collections
import statistics


class AccountDoesNotExist(Exception):
    pass


class InsufficientFunds(Exception):
    pass


class BlockDoesNotExist(Exception):
    pass


class Middleware:

    def __init__(self, accounts, genesis_hash):
        self.accounts = accounts.copy()
        self.genesis_hash = genesis_hash

        self.history = collections.deque([(accounts.copy(), genesis_hash, set())])

    def process_block(self, block_hash, prev_block_hash, transfers):
        account_updates = collections.defaultdict(int)

        txs = set()
        for transfer in transfers:
            amount = transfer['amount']
            sender = transfer['sender']
            receiver = transfer['receiver']
            account_updates[sender] -= amount
            account_updates[receiver] += amount

            # FIXME (sender, receiver, amount) serves as a transaction identifier here, for
            # demonstration purposes
            txs.add((sender, receiver, amount))

        self.update_accounts(block_hash, account_updates, txs, prev_block_hash)

    def update_accounts(self, block_hash, amounts, transaction_identifiers, prev_block_hash=None):
        if prev_block_hash is None:
            prev_block_hash = self.genesis_hash

        i = 0
        while True:
            if i == len(self.history):
                raise BlockDoesNotExist()

            historical_amounts, historical_block_hash, _ = self.history[i]
            if historical_block_hash != prev_block_hash:
                i += 1
                continue

            updated_amounts = {k: v for k, v in historical_amounts.items()}
            for account, amount in amounts.items():
                if account not in self.accounts:
                    raise AccountDoesNotExist()

                historical_amount = historical_amounts[account]
                if historical_amount + amount < 0:
                    raise InsufficientFunds()

                updated_amount = historical_amount + amount
                self.accounts[account] = updated_amount
                updated_amounts[account] = updated_amount

            for j in range(i):
                self.history.popleft()

            self.history.appendleft((updated_amounts, block_hash, transaction_identifiers))
            break

    def get_median(self):
        return statistics.median(self.accounts.values())

    def get_number_of_confirmations_since(self, tx_id):
        # FIXME expected `tx_id` is (sender, receiver, amount), for demonstration purposes

        for i, (a, b, txs) in enumerate(self.history):
            if tx_id in txs:
                return i + 1
        return 0
